<?php include("header.php") ?>

<div class="container">
	<ol class="breadcrumb">
		<li><a href="#">Subject</a></li>
		<li class="active">Create Minigame</li>        
	</ol>
</div>

<section class="subj-area crt-game-area clrlist">
	<div class="container">
		<div class="hed underline">
			<h2>Create a New miniGame</h2>
		</div>
		<div class="game-info col-sm-12">
			<form>
				<div class="form-group p0 col-sm-12">
					<label for="gameTitle">Title of Game</label>
					<input type="text" class="form-control" id="gameTitle" placeholder="Title of Game" />
				</div>
				<div class="form-group p0 col-sm-12">
					<label for="gameTitle">Description of Game</label>
					<input type="text" class="form-control" id="gameTitle" placeholder="Write your Game description" />
				</div>
				<div class="form-group game__add__timer hidden-checkbox p0 col-sm-12">
					<label for="addTimer">Add Timer</label>
					<input type="checkbox" id="toggle"/>
					<div class="checkylbl">
					  <label for="toggle"></label>
					</div>
					<div class="game__timer__box">
						<input type="text" class="form-control" id="addTimer" placeholder="45.00" />
					</div>
				</div>
				<button class="next-step"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>next step</button>
			</form>
		</div>
	</div>
</section>

<section class="crt-game-steps clrlist">
	<div class="container">
		<div class="accordion-area accordion-plus col-sm-12">
			<div class="panel-group" id="accordion">
				  <div class="panel panel-default active">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#chooseGameType">
						  choose Game type</a>
					  </h4>
					</div>
					<div id="chooseGameType" class="selector panel-collapse collapse in">
					  <div class="panel-body">
						<div class="game-type-lft selectme is-active col-sm-6">
							<div class="type__ind__locks chooseGameTypeClass" id="indi-type-locks">
								<ul>
									<li><i class="fa fa-lock"></i></li>
									<li><i class="fa fa-lock"></i></li>
									<li><i class="fa fa-lock"></i></li>
									<li><i class="fa fa-lock"></i></li>
								</ul>
								<h3>Individual Locks</h3>
								<h4>Puzzles can be solved in any order</h4>
							</div>
						</div>
						<div class="game-type-rgt selectme col-sm-6">
							<div class="type__ind__locks type__conn__locks chooseGameTypeClass" id="conn-type-locks">
								<ul>
									<li><i class="fa fa-lock"></i></li>
									<li><i class="fa fa-lock"></i></li>
									<li><i class="fa fa-lock"></i></li>
									<li><i class="fa fa-lock"></i></li>
								</ul>
								<h3>Connected Locks</h3>
								<h4>Sequential - One puzzle leads to another</h4>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="next-prev-step">
							<button class="prev-step"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>previous step</button>
							<button class="next-step"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>next step</button>
						</div>
						<div class="clearfix"></div>
					  </div>
					</div>
				  </div>
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#selectLockType" class="collapsed">
						  SELECT LOCK TYPE
						</a>
					  </h4>
					</div>
					<div id="selectLockType" class="panel-collapse collapse select-lock-type">
					  <div class="panel-body">
						<div class="row">
							<ul class="nav nav-pills">
								<li class="game-select-lock cols-5 active">
									<a data-toggle="pill" href="#selectLockText">
										<div class="select__lock__inr">
											<div class="select__lock__img">
												<img src="images/select-lock1.png" alt="select lock" />
											</div>
											<div class="select__lock__title">
												<h2>TEXT Lock</h2>
											</div>
										</div>
									</a>
								</li>
								<li class="game-select-lock cols-5">
									<a data-toggle="pill" href="#selectLockNumber">
										<div class="select__lock__inr">
											<div class="select__lock__img">
												<img src="images/select-lock2.png" alt="select lock" />
											</div>
											<div class="select__lock__title">
												<h2>NUMBER Lock</h2>
											</div>
										</div>
									</a>
								</li>
								<li class="game-select-lock cols-5">
									<a data-toggle="pill" href="#selectLockDirection">
										<div class="select__lock__inr">
											<div class="select__lock__img">
												<img src="images/select-lock3.png" alt="select lock" />
											</div>
											<div class="select__lock__title">
												<h2>dIRECTIOnAL Lock</h2>
											</div>
										</div>
									</a>
								</li>
								<li class="game-select-lock cols-5">
									<a data-toggle="pill" href="#selectLockShape">
										<div class="select__lock__inr">
											<div class="select__lock__img">
												<img src="images/select-lock4.png" alt="select lock" />
											</div>
											<div class="select__lock__title">
												<h2>SHAPE Lock</h2>
											</div>
										</div>
									</a>
								</li>
								<li class="game-select-lock cols-5">
									<a data-toggle="pill" href="#selectLockColor">
										<div class="select__lock__inr">
											<div class="select__lock__img">
												<img src="images/select-lock5.png" alt="select lock" />
											</div>
											<div class="select__lock__title">
												<h2>COLOR Lock</h2>
											</div>
										</div>
									</a>
								</li>
							</ul>
							<div class="select-lock__tabs col-sm-12">
								<div class="tab-content">
									<div id="selectLockText" class="select-lock__tabs--text tab-pane fade in active">
										<div class="lock__tabs--number-box">
											<div class="select-lock__tabs--number__select">
												<h3>Select the numbers that will open the lock</h3>
												<div class="tabs--number__select">
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>A</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>B</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>C</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>D</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>E</h2>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>F</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>G</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>H</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>I</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>J</h2>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>K</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>L</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>M</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>N</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>O</h2>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>P</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>Q</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>R</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>S</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>T</h2>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>U</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>V</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>W</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>X</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>Y</h2>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>Z</h2>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="select-lock__tabs--number__select select-lock__tabs--comb__select">
												<h3>Selected Combination</h3>
												<div class="tabs--number__select tabs--number__select--two">
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2>A</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2>B</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2>C</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2>D</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2>E</h2>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="edit-save-btns">
												<button class="edit-btn pul-lft">
												Edit
												</button>
												<button class="edit-btn pul-rgt">
												Save
												</button>
											</div>
										</div>
										<div class="clearfix"></div>	
									</div>
									<div id="selectLockNumber" class="select-lock__tabs--number tab-pane fade">
										<div class="lock__tabs--number-box">
											<div class="select-lock__tabs--number__select">
												<h3>Select the numbers that will open the lock</h3>
												<div class="tabs--number__select">
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>1</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>2</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>3</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>4</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>5</h2>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>6</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>7</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>8</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>9</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2>10</h2>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="select-lock__tabs--number__select select-lock__tabs--comb__select">
												<h3>Selected Combination</h3>
												<div class="tabs--number__select tabs--number__select--two">
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2>9</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2>0</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2>2</h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2> </h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr number__select-inr--comb">
															<h2> </h2>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="edit-save-btns">
												<button class="edit-btn pul-lft">
												Edit
												</button>
												<button class="edit-btn pul-rgt">
												Save
												</button>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div id="selectLockDirection" class="select-lock__tabs--direct tab-pane fade">
										<div class="lock__tabs--number-box">
											<div class="select-lock__tabs--number__select">
												<h3>Select the directions that will open the lock</h3>
												<div class="tabs--number__select tabs--number__select--direct">
													<div class="number__select-box col-sm-3">
														<div class="number__select-inr">
															<h2><i class="fa fa-arrow-up"></i></h2>
														</div>
													</div>
													<div class="number__select-box col-sm-3">
														<div class="number__select-inr">
															<h2><i class="fa fa-arrow-down" ></i></h2>
														</div>
													</div>
													<div class="number__select-box col-sm-3">
														<div class="number__select-inr">
															<h2><i class="fa fa-arrow-left" ></i></h2>
														</div>
													</div>
													<div class="number__select-box col-sm-3">
														<div class="number__select-inr">
															<h2><i class="fa fa-arrow-right" ></i></h2>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="select-lock__tabs--number__select select-lock__tabs--comb__select ">
												<h3>Selected Combination</h3>
												<div class="tabs--number__select tabs--number__select--two">
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2></h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2></h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2></h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2></h2>
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="number__select-inr">
															<h2></h2>
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="edit-save-btns">
												<button class="edit-btn pul-lft">
												Edit
												</button>
												<button class="edit-btn pul-rgt">
												Save
												</button>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div id="selectLockShape" class="select-lock__tabs--shape tab-pane fade">
										<div class="lock__tabs--number-box">
											<div class="select-lock__tabs--number__select">
												<h3>Select the shapes that will open the lock.</h3>
												<div class="tabs--number__select">
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-circle.png" alt="circle shape" />
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-square.png" alt="square shape" />
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-triangle.png" alt="triangle shape" />
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-star.png" alt="star shape" />
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-diamond.png" alt="circle shape" />
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="select-lock__tabs--number__select select-lock__tabs--comb__select">
												<h3>Selected Combination</h3>
												<div class="tabs--number__select tabs--number__select--two">
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-circle.png" alt="circle shape" />
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-square.png" alt="square shape" />
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-triangle.png" alt="triangle shape" />
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-star.png" alt="star shape" />
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box">
															<img src="images/shape-diamond.png" alt="circle shape" />
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="edit-save-btns">
												<button class="edit-btn pul-lft">
												Edit
												</button>
												<button class="edit-btn pul-rgt">
												Save
												</button>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div id="selectLockColor" class="select-lock__tabs--color tab-pane fade">
										<div class="lock__tabs--number-box">
											<div class="select-lock__tabs--number__select">
												<h3>Select the colors in the sequence that you wish to open the lock</h3>
												<div class="tabs--number__select">
													<div class="number__select-box cols-5">
														<div id="select-color-red" class="select-color-box">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div id="select-color-green" class="select-color-box">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div id="select-color-orange" class="select-color-box">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div id="select-color-blue" class="select-color-box">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div id="select-color-brown" class="select-color-box">
															
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="number__select-box cols-5">
														<div id="select-color-yellow" class="select-color-box">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div id="select-color-purple" class="select-color-box">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div id="select-color-grey" class="select-color-box">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div id="select-color-white" class="select-color-box">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div id="select-color-black" class="select-color-box">
															
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="select-lock__tabs--number__select select-lock__tabs--comb__select">
												<h3>Selected Combination</h3>
												<div class="tabs--number__select tabs--number__select--two">
													<div class="number__select-box cols-5">
														<div class="shape__select-box shape__select-box--circle" id="select-color-yellow">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box shape__select-box--circle" id="select-color-purple">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box shape__select-box--circle" id="select-color-grey">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box shape__select-box--circle" id="select-color-blue">
															
														</div>
													</div>
													<div class="number__select-box cols-5">
														<div class="shape__select-box shape__select-box--circle" id="select-color-orange">
															
														</div>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="clearfix"></div>
											<div class="edit-save-btns">
												<button class="edit-btn pul-lft">
												Edit
												</button>
												<button class="edit-btn pul-rgt">
												Save
												</button>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="next-prev-step">
							<button class="prev-step"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i>previous step</button>
							<button class="next-step"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i>next step</button>
						</div>
					  </div>
					</div>
				  </div>
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#lockSetup" class="collapsed">
						  LOCK SETUP / STORY
						</a>
					  </h4>
					</div>
					<div id="lockSetup" class="panel-collapse collapse lock-setup-story">
					  <div class="panel-body">
							<h3>Select an item that will kick off this particular lock.<br /> This content will show about the lock that the user is working on opening.</h3>
							<div class="row">
								<ul class="nav nav-pills">
									<li class="lock-setup-box text-center col-sm-4 active">
										<a data-toggle="pill" href="#lockSetupImageTab">
											<div class="lock-setup__inr">
												<div class="lock-setup__icon">
													<i class="fa fa-picture-o"></i>
												</div>
												<div class="lock-setup__title">
													<h2>Image</h2>
												</div>
											</div>
										</a>
									</li>
									<li class="lock-setup-box text-center col-sm-4">
										<a data-toggle="pill" href="#lockSetupTab">
											<div class="lock-setup__inr">
												<div class="lock-setup__icon">
													<i class="fa fa-font"></i>
												</div>
												<div class="lock-setup__title">
													<h2>Text</h2>
												</div>
											</div>
										</a>
									</li>
									<li class="lock-setup-box text-center col-sm-4">
										<a data-toggle="pill" href="#lockSetupVideoTab">
											<div class="lock-setup__inr">
												<div class="lock-setup__icon">
													<i class="fa fa-video-camera"></i>
												</div>
												<div class="lock-setup__title">
													<h2>Video URL</h2>
												</div>
											</div>
										</a>
									</li>
								</ul>
								<div class="lock-setup__tabs col-sm-12">
									<div class="tab-content">
										<div id="lockSetupImageTab" class="lock-setup__tabs--img tab-pane fade in active">
											<div class="lock-setup__img--inr is-active text-center">
											  <h3>Drag and Drop Image File Here</h3>
											  <div class="lock-setup__img--box">
												<img src="images/upload-icon.png" alt="lock image" />
											  </div>
											  <div class="lock-setup__img--file">
												<input type="file" id="lockImageInput" onchange="lockSetupImage(this);"  />
												<button>SELECT A FILE FROM YOUR COMPUTER</button>
											  </div>
											</div>
											<div class="lock-setup__img--has-image">
												<div class="lock-setup__img__remove col-sm-2">
													<h3><span class="remove-image">&times;</span>Remove</h3>
												</div>
												<div class="lock-setup__img__preview col-sm-6 col-sm-offset-1">
													<img src="images/upload-icon.png" id="lockImageUploaded" alt="lock image" />
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<div id="lockSetupTab" class="tab-pane fade">
											<div class="lock-setup__text--box text-left">
												<form>
													<div class="form-group">
													  <label for="lockSetupTextNeeded">Enter the text that will be needed to open the lock.</label>
													  <textarea class="form-control" rows="5" id="lockSetupTextNeeded" placeholder="Enter the answer text here"></textarea>
													</div>
												</form>
											</div>
										</div>
										<div id="lockSetupVideoTab" class="tab-pane fade">
											<div class="lock-setup__text--box text-left">
												<form>
													<div class="form-group">
													  <label for="lockSetupTextNeeded">Enter URL Video here</label>
													  <textarea class="form-control" rows="5" id="lockSetupTextNeeded" placeholder="Enter the video url here"></textarea>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						<div class="next-prev-step">
							<button class="prev-step"><i class="fa fa-chevron-circle-left" ></i>previous step</button>
							<button class="next-step"><i class="fa fa-chevron-circle-right" ></i>next step</button>
						</div>
						<div class="clearfix"></div>
						<div class="finish-save-add-btn">
							<button class="prev-step"><i class="fa fa-arrow-circle-o-down" ></i>FINISH AND SAVE</button>
							<button class="next-step"><i class="fa fa-plus-circle"></i></i>ADD ANOTHER LOCK</button>
						</div>
						<div class="clearfix"></div>
					  </div>
					</div>
				  </div>
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#endGameScreen">
						  End of Game Screen</a>
					  </h4>
					</div>
					<div id="endGameScreen" class="panel-collapse collapse">
					  <div class="panel-body">
						<div class="end-screen-box text-center">
							<div class="end-screen__inr">
								<h2>My First miniGame</h2>
								<h3>Congratulations!</h3>
								<h3>You Broke Out in XX:XX</h3>
								<div class="end-screen__logo">
									<img src="images/end-screen-logo.png" alt="logo" />
								</div>
								<h5>SHARE YOUR SUCCESS!</h5>
								<div class="end-screen-social">
									<ul>
										<li class="signup__connect signup__connect--twitter p0 col-sm-4">
											<a href="#"><i class="fa fa-twitter"></i>Twitter</a>
										</li>
										<li class="signup__connect signup__connect--fb p0 col-sm-4">
											<a href="#"><i class="fa fa-facebook"></i>Facebook</a>
										</li>
										<li class="signup__connect signup__connect--google p0 col-sm-4">
											<a href="#"><i class="fa fa-google-plus"></i>Google</a>
										</li>
										<div class="clearfix"></div>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
					  </div>
					</div>
				  </div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	
	function lockSetupImage(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#lockImageUploaded')
                    .attr('src', e.target.result)
                    .width('430px')
                    .height('320px');
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
	
	
	
	
	
</script>

<script>
	$(function() {
     $("#lockImageInput").change(function (){
       var fileName = $(this).val();
       $(".lock-setup__img--inr").removeClass('is-active');
	   $(".lock-setup__img--has-image").addClass('is-active');
     });
  });

</script>

<script>
	$(document).ready(function(){
		$('.remove-image').click(function(){
			$(".lock-setup__img--has-image").removeClass('is-active');
			$(".lock-setup__img--inr").addClass('is-active');
		});
	});
	
</script>



<script type="text/javascript">
$(document).ready(function(){
   	$(".selector .selectme").on("click", function() {
	   $(this).siblings(".selectme").removeClass("is-active");
		$(this).addClass("is-active");
	});
		});
</script>


	
	
	

	


<?php include("footer.php") ?>