<?php include("header.php") ?>

<section class="subj-area games-area--mini clrlist">
		<div class="container">
			<div class="hed underline">
				<h2>Mini Games</h2>
			</div>
			<div class="subj__search col-sm-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input id="search" type="text" class="form-control" name="search" placeholder="Search games">
				</div>
			</div>
			<div class="feat-games col-sm-12">
				<div class="container0">
					<div class="feat__hed col-sm-12">
						<div class="hed underline">
							<h3>Featured miniGames</h3>
						</div>
					</div>
					<div class="feat-box col-sm-6">
						<div class="feat__inr">
							<div class="feat__img">
								<img src="images/minigames1.jpg" alt="mini games" />
							</div>
							<div class="feat__title">
								<h4>the Faculty Meeting</h4>
							</div>
							<div class="feat__bar">
								<ul>
									<li class="feat__game__share"><a href="#" data-toggle="modal" data-target="#shareGame"><i class="fa fa-share-alt"></i>Share Game</a></li>
									<li class="feat__game__start"><a href="#" data-toggle="modal" data-target="#startGame"><i class="fa fa-play"></i>Start Game</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="feat-box col-sm-6">
						<div class="feat__inr">
							<div class="feat__img">
								<img src="images/minigames2.jpg" alt="mini games" />
							</div>
							<div class="feat__title">
								<h4>Attack of the locks</h4>
							</div>
							<div class="feat__bar">
								<ul>
									<li class="feat__game__share"><a href="#" data-toggle="modal" data-target="#shareGame"><i class="fa fa-share-alt"></i>Share Game</a></li>
									<li class="feat__game__start"><a href="#" data-toggle="modal" data-target="#startGame"><i class="fa fa-play"></i>Start Game</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	
	<section class="create-area clrlist">
		<div class="container">
			<div class="create-game col-sm-12">
				<div class="create__game__box">
					<a href="#"><span>&#43;</span></a>
					<h2>Create New Breakout Edu Minigame</h2>
				</div>
			</div>
		</div>
	</section>
	
	<section class="feat-games feat-games--edit clrlist">
		<div class="container">
			<div class="feat-box col-sm-6">
				<div class="feat__inr">
					<div class="feat__img">
						<img src="images/minigames3.jpg" alt="mini games" />
					</div>
					<div class="feat__title">
						<h4>Dr. Bore and the Quest For Hope</h4>
					</div>
					<div class="feat__edit__icon">
						<a href="#"><i class="fa fa-pencil"></i></a>
					</div>
					<div class="feat__bar">
						<ul>
							<li class="feat__game__share"><a href="#" data-toggle="modal" data-target="#shareGame"><i class="fa fa-share-alt"></i>Share Game</a></li>
							<li class="feat__game__start"><a href="#" data-toggle="modal" data-target="#startGame"><i class="fa fa-play"></i>Start Game</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="feat-box col-sm-6">
				<div class="feat__inr">
					<div class="feat__img">
						<img src="images/minigames4.jpg" alt="mini games" />
					</div>
					<div class="feat__title">
						<h4>Time Warp</h4>
					</div>
					<div class="feat__edit__icon">
						<a href="#"><i class="fa fa-pencil"></i></a>
					</div>
					<div class="feat__bar">
						<ul>
							<li class="feat__game__share"><a href="#" data-toggle="modal" data-target="#shareGame"><i class="fa fa-share-alt"></i>Share Game</a></li>
							<li class="feat__game__start"><a href="#" data-toggle="modal" data-target="#startGame"><i class="fa fa-play"></i>Start Game</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	

	


<?php include("footer.php") ?>