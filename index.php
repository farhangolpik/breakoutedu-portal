<?php include("header.php") ?>

<section class="subj-area clrlist">
		<div class="container">
			<div class="hed underline">
				<h2>Subject Packs</h2>
			</div>
			<div class="subj__search col-sm-12">
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					<input id="search" type="text" class="form-control" name="search" placeholder="Search games">
				</div>
			</div>
			<div class="subj-main col-sm-12">
			<!-- Swiper -->
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj1.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>Chemistry Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>10</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj2.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>History Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>10</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj3.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>Geomtery Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>10</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj4.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>science Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>20</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj5.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>mathematics Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>30</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj6.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>biology Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>5</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj1.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>Chemistry Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>10</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj2.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>History Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>10</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj3.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>Geomtery Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>10</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="subj-box">
							<div class="subj__inr">
								<div class="subj__img">
									<img src="images/subj4.jpg" alt="subject" />
								</div>
								<div class="subj__title">
									<h4>science Games</h4>
								</div>
								<div class="subj__games__num">
									<h4>10</h4>
									<span>Games</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Add Pagination -->
				<div class="swiper-pagination"></div>
			</div>
			</div>
			
		</div>
	</section>

	<section class="games-area clrlist">
		<div class="container">
			<div class="hed underline">
				<h2>Mini Games</h2>
			</div>
		</div>
		<div class="container0">
			<div class="feat-games">
				<div class="container">
					<div class="feat__hed col-sm-12">
						<div class="hed underline">
							<h3>Featured miniGames</h3>
						</div>
					</div>
					<div class="feat-box col-sm-6">
						<div class="feat__inr">
							<div class="feat__img">
								<img src="images/minigames1.jpg" alt="mini games" />
							</div>
							<div class="feat__title">
								<h4>the Faculty Meeting</h4>
							</div>
							<div class="feat__bar">
								<ul>
									<li class="feat__game__share"><a href="#" data-toggle="modal" data-target="#shareGame"><i class="fa fa-share-alt"></i>Share Game</a></li>
									<li class="feat__game__start"><a href="#" data-toggle="modal" data-target="#startGame"><i class="fa fa-play"></i>Start Game</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="feat-box col-sm-6">
						<div class="feat__inr">
							<div class="feat__img">
								<img src="images/minigames2.jpg" alt="mini games" />
							</div>
							<div class="feat__title">
								<h4>Attack of the locks</h4>
							</div>
							<div class="feat__bar">
								<ul>
									<li class="feat__game__share"><a href="#" data-toggle="modal" data-target="#shareGame"><i class="fa fa-share-alt"></i>Share Game</a></li>
									<li class="feat__game__start"><a href="#" data-toggle="modal" data-target="#startGame"><i class="fa fa-play"></i>Start Game</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="create-area clrlist">
		<div class="container">
			<div class="create-game col-sm-12">
				<div class="create__game__box">
					<a href="#"><span>&#43;</span></a>
					<h2>Create New Breakout Edu Minigame</h2>
				</div>
			</div>
		</div>
	</section>
	


<?php include("footer.php") ?>