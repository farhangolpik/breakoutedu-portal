<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Home</title>

	<link rel="icon" type="image/png" href="images/favicon.png">
	
    <!-- Bootstrap --><link href="css/bootstrap.min.css" rel="stylesheet">
	
	<link rel="stylesheet" href="css/stylized.css">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/colorized.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/slidenav.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
		<!--link rel="stylesheet" href="css/foundation-icons.css"-->
		<!--link rel="stylesheet" type="text/css" href="css/flaticon.css"--> 
	<link rel="stylesheet" href="css/swiper.min.css">

	<!-- jQuery -->
	<!--[if (!IE)|(gt IE 8)]><!-->
	  <script src="js/jquery-2.2.4.min.js"></script>
	<!--<![endif]-->


	<!--[if lte IE 8]>
	  <script src="js/jquery1.9.1.min.js"></script>
	<![endif]-->
	
	<!--browser selector-->
	<script src="js/css_browser_selector.js" type="text/javascript"></script>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	
	<!--scrollreveal-->
	<script src="extralized/scrollreveal.min.js"></script>
	<script>
	//	window.scrollReveal = new scrollReveal();
	</script>
	<!--./scrollreveal-->
	
	<!--modernizr-->
	<script src="extralized/modernizr.min.js"></script>
	
	
	<!--DatePicker-->
	<script src="extralized/bootstrap-datepicker.js"></script>

	
	<!--datetimepicker-->
	<script type="text/javascript" src="extralized/moment.min.js"></script>
	<script type="text/javascript" src="extralized/bootstrap-datetimepicker.js"></script><!--//required moment.js-->
	<link href='extralized/bootstrap-datetimepicker.css' rel='stylesheet' />
		<script type="text/javascript">
			jQuery(function () {
				jQuery('#datetimepicker1').datetimepicker();
				});
		</script>
	<!--./datetimepicker-->


	<!--daterangepicker-->
	<link rel="stylesheet" href="extralized/daterangepicker.css">
	<script src="extralized/daterangepicker.js"></script>
	<!--./daterangepicker-->


	<!--highcharts-->
	<script type="text/javascript" src="extralized/highcharts.js"></script>
	<!--./highcharts-->


	<!--Chartist-->
		<link rel="stylesheet" href="extralized/chartist.min.css">
		<script src="extralized/chartist.min.js"></script>
	<!--./Chartist-->
	

  </head>
  <body class="transition nav-plusminus slide-navbar slide-navbar--right">

<header>
	<section class="hdr-area hdr-nav hdr--sticky cross-toggle bg-white">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<a class="navbar-brand" href="./"><img src="images/logo.png" alt="logo" class="broken-image"/></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  <ul class="nav navbar-nav navbar-main pul-rgt">
						<li><a href="index.php">Home</a></li>
						<li class=""><a href="#!2">Store</a></li>
						<li><a href="#!3">Search</a></li>
						<li style="display:none;"><a href="#!6">My Account</a></li>
						<li><a data-toggle="modal" data-target="#login">Login</a></li>
						<li><a data-toggle="modal" data-target="#signup">Sign Up</a></li>
					  </ul>
					
					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>

<!-- Sign up Modal -->
<div id="signup" class="modal fade signup-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Create an Account</h4>
      </div>
      <div class="modal-body">
		<div class="signup-form modal-form">
			<form>
			  <div class="form-group p0 col-sm-4">
				<label for="firstName">First Name :</label>
				<input type="text" class="form-control" id="firstName" placeholder="First Name" />
			  </div>
			  <div class="form-group p0 col-sm-4">
				<label for="lastName">Last Name :</label>
				<input type="text" class="form-control" id="lastName" placeholder="Last Name" />
			  </div>
			  <div class="form-group p0 col-sm-4">
				<label for="firstName">Title :</label>
				<select class="form-control" id="title">
					<option></option>
					<option></option>
					<option></option>
				</select>
			  </div>
			  <div class="clearfix"></div>
			  <div class="form-group p0 col-sm-12">
				<label for="email">Email Address :</label>
				<input type="email" class="form-control" id="email" placeholder="Email Address" />
			  </div>
			  <div class="clearfix"></div>
			  <div class="form-group p0 col-sm-6">
				<label for="password">Password :</label>
				<input type="password" class="form-control" id="password" placeholder="Password" />
			  </div>
			  <div class="form-group p0 col-sm-6">
				<label for="confirmPassword">Confirm Password :</label>
				<input type="password" class="form-control" id="confirmPassword" placeholder="Confirm Password" />
			  </div>
			  <div class="clearfix"></div>
			  <div class="form-group p0 col-sm-12">
				<label for="accessCode">Access Code :</label>
				<input type="text" class="form-control" id="accessCode" placeholder="Access Code" />
			  </div>
			  <div class="clearfix"></div>
			  <div class="has-access-code text-center p0 col-sm-12">
				<a href="#"><i>Don’t have an access code?</i></a>
			  </div>
			  <div class="clearfix"></div>
			  <div class="signup-option-or p0 col-sm-12">
				<div class="hed crossline">
					<h4>OR</h4>
					<hr>
				</div>
			  </div>
			  <div class="clearfix"></div>
			  <div class="signup__connect signup__connect--twitter p0 col-sm-4">
				<button><i class="fa fa-twitter"></i>Twitter</button>
			  </div>
			  <div class="signup__connect signup__connect--fb p0 col-sm-4">
				<button><i class="fa fa-facebook"></i>Facebook</button>
			  </div>
			  <div class="signup__connect signup__connect--google p0 col-sm-4">
				<button><i class="fa fa-google-plus"></i>Google</button>
			  </div>
			  <div class="clearfix"></div>
			  <button type="submit" class="signup__submit">Sign Up <i class="fa fa-angle-right"></i></button>
			</form>
		</div>
      </div>
    </div>

  </div>
</div>
<!--Sign up Modal End-->

<!-- Login Modal -->
<div id="login" class="modal fade login-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Breakout EDU platform Access Required</h4>
      </div>
      <div class="modal-body">
		<div class="login-form modal-form">
			<form>
			  <div class="form-group text-center col-sm-12">
				<label for="accessCode">Access Code :</label>
				<input type="text" class="form-control" id="accessCode" placeholder="Access Code" />
			  </div>
			  <div class="clearfix"></div>
			  <div class="has-access-code text-center col-sm-12">
				<a href="#"><i>Don’t have an access code?</i></a>
			  </div>
			  <div class="clearfix"></div>
			  <div class="col-sm-12">
				<button type="submit" class="signup__submit">Submit <i class="fa fa-angle-right"></i></button>
			  </div>
			  <div class="clearfix"></div>
			</form>
		</div>
      </div>
    </div>

  </div>
</div>
<!--Login Modal End-->

<!-- Share Game Modal -->
<div id="shareGame" class="modal fade shareGame-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Share “The Faculty Meeting”</h4>
      </div>
      <div class="modal-body">
		<div class="share-game-form modal-form">
			<form>
			  <div class="form-group col-sm-9">
				<label for="gameUrl">URL :</label>
				<input type="text" class="form-control" id="gameUrl" placeholder="Enter URL" />
			  </div>
			  <div class="form-group share__copy__btn col-sm-3">
				<button>copy URL</button>
			  </div>
			  <div class="clearfix"></div>
			  <div class="form-group col-sm-9">
				<label for="playcode">Play Code :</label>
				<input type="text" class="form-control" id="playcode" placeholder="Enter Play Code" />
			  </div>
			  <div class="form-group share__copy__btn col-sm-3">
				<button>copy CODE</button>
			  </div>
			  <div class="clearfix"></div>
			  
			  <div class="add-lib-btn col-sm-12">
				<button><i class="fa fa-folder-open"></i>ADD TO MINIGAME LIBRARY</button>
			  </div>
			  <div class="clearfix"></div>
			  <div class="share__social col-sm-12">
				  <div class="signup__connect signup__connect--twitter p0 col-sm-4">
					<button><i class="fa fa-twitter"></i>Twitter</button>
				  </div>
				  <div class="signup__connect signup__connect--fb p0 col-sm-4">
					<button><i class="fa fa-facebook"></i>Facebook</button>
				  </div>
				  <div class="signup__connect signup__connect--google p0 col-sm-4">
					<button><i class="fa fa-google-plus"></i>Google</button>
				  </div>
			  </div>
			  <div class="clearfix"></div>
			</form>
		</div>
      </div>
    </div>

  </div>
</div>
<!--Share Game Modal End-->

<!-- Share Game Modal -->
<div id="startGame" class="modal fade startGame-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">The Faculty Meeting</h4>
      </div>
      <div class="modal-body">
		<div class="share-game-form modal-form">
			<form>
			  <div class="form-group col-sm-12">
				<label for="gameUrl">Name :</label>
				<input type="text" class="form-control" id="gameUrl" placeholder="Enter your Name" />
			  </div>
			  <div class="clearfix"></div>
			  <div class="form-group col-sm-12">
				<label for="playcode">Play Code :</label>
				<input type="text" class="form-control" id="playcode" placeholder="Enter Play Code" />
			  </div>
			  <div class="clearfix"></div>
			  
			  <div class="add-lib-btn start-game-btn col-sm-12">
				<button><i class="fa fa-play"></i>Start game</button>
			  </div>
			  <div class="clearfix"></div>
			</form>
		</div>
      </div>
    </div>

  </div>
</div>
<!--Share Game Modal End-->

<main id="page-content">