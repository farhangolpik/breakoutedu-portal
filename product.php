<?php include("header.php") ?>

<div class="container">
	<ol class="breadcrumb">
		<li><a href="#">Subject Packs</a></li>
		<li><a href="#">Science</a></li>
		<li class="active">Chemistry</li>        
	</ol>
</div>
<section class="prod-bnr-area chem-bnr">
	<div class="container">
		<div class="prod__bnr__cont valigner text-center col-sm-12">
			<div class="valign">
				<div class="hed underline">
					<h2>Chemistry</h2>
				</div>
				<div class="subj__games__num">
					<h4>10</h4>
					<span>Games</span>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="prod-area clrlist">
	<div class="container">
		<div class="prod-box col-sm-4">
			<div class="prod__inr">
				<div class="prod__img">
					<img src="images/prod1.jpg" alt="chemistry" />
				</div>
				<div class="prod__title">
					<h4>chemistry Game 1</h4>
				</div>
			</div>
		</div>
		<div class="prod-box col-sm-4">
			<div class="prod__inr">
				<div class="prod__img">
					<img src="images/prod2.jpg" alt="chemistry" />
				</div>
				<div class="prod__title">
					<h4>chemistry Game 2</h4>
				</div>
			</div>
		</div>
		<div class="prod-box col-sm-4">
			<div class="prod__inr">
				<div class="prod__img">
					<img src="images/prod3.jpg" alt="chemistry" />
				</div>
				<div class="prod__title">
					<h4>chemistry Game 3</h4>
				</div>
			</div>
		</div>
		<div class="prod-box col-sm-4">
			<div class="prod__inr">
				<div class="prod__img">
					<img src="images/prod4.jpg" alt="chemistry" />
				</div>
				<div class="prod__title">
					<h4>chemistry Game 4</h4>
				</div>
			</div>
		</div>
		<div class="prod-box col-sm-4">
			<div class="prod__inr">
				<div class="prod__img">
					<img src="images/prod5.jpg" alt="chemistry" />
				</div>
				<div class="prod__title">
					<h4>chemistry Game 5</h4>
				</div>
			</div>
		</div>
		<div class="prod-box col-sm-4">
			<div class="prod__inr">
				<div class="prod__img">
					<img src="images/prod6.jpg" alt="chemistry" />
				</div>
				<div class="prod__title">
					<h4>chemistry Game 6</h4>
				</div>
			</div>
		</div>
	</div>
</section>


	


<?php include("footer.php") ?>